$(function() {

  var // Define maximum number of files.
      max_file_number = 4,
      // Define your form id or class or just tag.
      $form = $('.my-post-form'),
      // Define your upload field class or id or tag.
      $file_upload = $('.upload-post-image', $form);


  $file_upload.on('change', function () {
    var number_of_images = $(this)[0].files.length;
    if (number_of_images > max_file_number) {
      alert(`You can upload maximum ${max_file_number} images.`);
      $(this).val('');
    }
  });
});

$(document).ready(function () {

        // like and unlike click
        $(".like-btn").click(function () {
            var post_id = this.id.split("_")[1];  // post_id
            this.classList.toggle('btn-danger');
            this.classList.toggle('btn-outline-danger');

            // AJAX Request
            $.ajax({
                url: '/like',
                type: 'POST',
                data: {post_id: post_id},
                dataType: 'json',
                success: function (response) {
                    console.log('like/unlike happened')
                }
            });

        });

    });