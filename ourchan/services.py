import os
from random import randint
from typing import List
import mimetypes

from sqlalchemy import text
from werkzeug.security import generate_password_hash

from . import db
from .models import User, Post
from .settings import MEDIA_DIR

mimetypes.init()


class WrongExtensionForImageError(Exception):
    pass


def get_friends_ids(user_id: int) -> List[int]:
    with db.engine.connect() as con:
        friend_ids = con.execute(text(
            """WITH friends_list AS (
                (SELECT follower_id, following_id 
                FROM user_user_sub)
                intersect 
                (SELECT following_id, follower_id
                FROM user_user_sub)
                )
                SELECT following_id FROM friends_list
                WHERE follower_id = :user_id;"""
        ), user_id=user_id)

    return friend_ids


def get_friends(user_id: int) -> List[User]:
    friends_ids = get_friends_ids(user_id)
    friends = User.query.filter(id in friends_ids).all()
    return friends


def get_users_top():
    with db.engine.connect() as con:
        users_top = con.execute(
            text("""
                SELECT * FROM (
                SELECT id, CONCAT(first_name, ' ', last_name) as name, 
                nickname, profile_picture, net_worth, gender, date_of_birth, 
                RANK() OVER (ORDER BY net_worth desc) AS rnk  FROM "user"
                ) as top
                ORDER BY rnk LIMIT 20""")
        ).all()
        return users_top


def search_users_query(user_name):
    with db.engine.connect() as con:
        users = con.execute(
            text("""
                SELECT * FROM "user" 
                WHERE LOWER(CONCAT(first_name, ' ', last_name)) LIKE 
                LOWER(:user_name_regexp) OR 
                LOWER(CONCAT(last_name, ' ', first_name)) LIKE 
                LOWER(:user_name_regexp);
                """),
            user_name_regexp=f"%{user_name}%"
        )
        print(f"{type(users)}")
        return users


def get_news_ids(user_id):
    with db.engine.connect() as con:
        news_list = con.execute(text("""
        SELECT p.id FROM user_user_sub uus
        INNER JOIN post p ON uus.following_id = post.user_id
        WHERE uus.follower_id = :user_id
        AND p.root_id IS NULL
        ORDER BY 
        """), user_id=user_id).all()
    news_list = list(map(lambda x: x[0], news_list))
    return news_list


def get_news(user_id):
    news_ids = get_news_ids(user_id)
    news_list = Post.query.filter(Post.id in news_ids).all()
    return news_list


def save_and_get_path(file):
    file_name, file_ext = file.filename.rsplit('.', 1)
    if mimetypes.types_map['.'+file_ext].split('/', 1)[0] != 'image':
        raise WrongExtensionForImageError
    path = os.path.join(
        MEDIA_DIR,
        f"{generate_password_hash(f'{file_name}-{randint(1, 10000)}')}"
        f".{file_ext}"
    )
    file.save(os.path.join('ourchan', 'static', path))
    return path
