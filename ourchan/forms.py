from wtforms import Form, BooleanField, StringField, PasswordField, validators
from wtforms.widgets import TextInput


class AuthorizationForm(Form):
    email = StringField('Email', [validators.DataRequired(),
                                  validators.Email()])
    password = PasswordField('Password', [validators.DataRequired()])
    remember = BooleanField('remember me')
