from typing import List, Tuple

from flask_login import UserMixin
from sqlalchemy import func, text
from sqlalchemy.orm import backref

from . import db


class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True), default=func.now(),
                           server_default=func.now(), onupdate=func.now())


class User(UserMixin, BaseModel):
    __tablename__ = 'user'
    email = db.Column(db.String(300), unique=True, nullable=False)
    password = db.Column(db.String(300), nullable=False)
    nickname = db.Column(db.String(300), unique=True, nullable=False)
    first_name = db.Column(db.String(200), nullable=False)
    last_name = db.Column(db.String(200), nullable=False)
    date_of_birth = db.Column(db.Date, nullable=False)
    profile_picture = db.Column(db.String(1000), default='media/user.svg',
                                server_default='media/user.svg')
    net_worth = db.Column(db.BigInteger, default='0', server_default='0')
    gender = db.Column(db.String(50), default="unknown")
    posts = db.relationship("Post", back_populates="user",
                            cascade="all, delete", passive_deletes=True)
    likes = db.relationship("Like", back_populates="user")
    games = db.relationship("Game", back_populates="designer")
    gambling_transactions = db.relationship("GamblingTransaction",
                                            back_populates="user")

    def get_friends_ids(self) -> List[Tuple[int]]:
        with db.engine.connect() as con:
            friend_ids = con.execute(text(
                """WITH friends_list AS (
                (SELECT follower_id, following_id 
                FROM user_user_sub)
                intersect 
                (SELECT following_id, follower_id
                FROM user_user_sub)
                )
                SELECT following_id FROM friends_list
                WHERE follower_id = :user_id;"""
            ), user_id=self.id).all()

        friends_ids = [friend_id[0] for friend_id in friend_ids]
        return friends_ids

    def get_friends(self):
        friends_ids = self.get_friends_ids()
        friends = [User.query.filter_by(id=friend_id).first() for friend_id in
                   friends_ids]
        return friends

    def get_all_followings_ids(self):
        with db.engine.connect() as con:
            followings = con.execute(
                text("""
                select following_id from 
                user_user_sub where follower_id = :user_id"""),
                user_id=self.id).all()
        all_followings_ids = \
            [following_id[0] for following_id in followings]
        return all_followings_ids

    def get_followings_ids(self):
        all_followers_ids = set(self.get_all_followers_ids())
        all_followings_ids = set(self.get_all_followings_ids())
        followings_ids = list(all_followings_ids.difference(all_followers_ids))
        return followings_ids

    def get_followings(self):
        followings_ids = self.get_followings_ids()
        followings = [User.query.filter_by(id=following_id).first() for
                      following_id in followings_ids]
        return followings

    def get_all_followers_ids(self):
        with db.engine.connect() as con:
            followers = con.execute(
                text("""
                select follower_id from 
                user_user_sub where following_id = :user_id"""),
                user_id=self.id).all()
        all_followers_ids = \
            [follower_id[0] for follower_id in followers]
        return all_followers_ids

    def get_followers_ids(self):
        all_followers_ids = set(self.get_all_followers_ids())
        all_followings_ids = set(self.get_all_followings_ids())
        followers_ids = list(all_followers_ids.difference(all_followings_ids))
        return followers_ids

    def get_followers(self):
        followers_ids = self.get_followers_ids()
        followers = [User.query.filter_by(id=follower_id).first() for
                     follower_id in followers_ids]
        return followers

    def get_name(self):
        return f"{self.first_name} {self.last_name}"

    def get_rname(self):
        return f"{self.last_name} {self.first_name}"

    def get_posts(self):
        posts = Post.query.filter_by(user_id=self.id, root_id=None).order_by(
            Post.created_at.desc()).all()
        return posts

    def liked(self, post_id):
        like = Like.query.filter_by(user_id=self.id, post_id=post_id).first()
        return like is not None

    def get_news_ids(self):
        with db.engine.connect() as con:
            news_list = con.execute(text("""
            SELECT p.id, (COALESCE((
            (select COALESCE((select count("like".id) from "like"
                where "like".post_id = p.id
                group by post_id), 0))
            + 3*(select count(post.id) from post where post.root_id = 
            p.id)
            ) / (EXTRACT(EPOCH FROM NOW() - p.created_at)), 0)) as relevance 
                FROM user_user_sub uus
            INNER JOIN post p ON uus.following_id = p.user_id
            WHERE uus.follower_id = :user_id
            AND p.root_id IS NULL
            ORDER BY relevance DESC;
            """), user_id=self.id).all()
        news_list = list(map(lambda x: x[0], news_list))
        return news_list

    def get_news(self):
        news_ids = self.get_news_ids()
        news_list = [Post.query.filter_by(id=i).first() for i in news_ids]
        return news_list


class Post(BaseModel):
    __tablename__ = 'post'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    text = db.Column(db.String(5000))
    user_id = db.Column(db.BigInteger, db.ForeignKey('user.id',
                                                     ondelete="CASCADE"))
    replying_to_post_id = db.Column(
        db.BigInteger, db.ForeignKey('post.id', ondelete="CASCADE"),
        nullable=True)
    root_id = db.Column(db.BigInteger, db.ForeignKey('post.id',
                                                     ondelete="CASCADE"),
                        nullable=True)
    replying_to_post = db.relationship("Post", remote_side=[id],
                                       backref=backref('replies'),
                                       foreign_keys=[replying_to_post_id])
    root = db.relationship("Post", remote_side=[id], backref=backref(
        'replies_on_root'), foreign_keys=[root_id])
    user = db.relationship("User", back_populates="posts")
    images = db.relationship("Image", back_populates="post")
    likes = db.relationship("Like", back_populates="post")

    def get_comments(self):
        comments = Post.query.filter_by(root_id=self.id).order_by(
            Post.created_at).all()
        return comments

    def get_replies(self):
        replies = Post.query.filter_by(replying_to_post_id=self.id).order_by(
            Post.created_at).all()
        return replies


class UserUserSub(BaseModel):
    __tablename__ = 'user_user_sub'
    follower_id = db.Column(db.BigInteger, db.ForeignKey('user.id',
                                                         ondelete="CASCADE"))
    following_id = db.Column(db.BigInteger, db.ForeignKey('user.id',
                                                          ondelete="CASCADE"))
    follower = db.relationship("User", foreign_keys=[follower_id])
    following = db.relationship("User", foreign_keys=[following_id])


class Image(BaseModel):
    __tablename__ = 'image'
    post_id = db.Column(db.BigInteger, db.ForeignKey('post.id'))
    file = db.Column(db.String(1000), nullable=False)
    post = db.relationship("Post", back_populates="images")


class Like(db.Model):
    __tablename__ = 'like'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    user_id = db.Column(db.BigInteger, db.ForeignKey('user.id',
                                                     ondelete="NO ACTION"))
    post_id = db.Column(db.BigInteger, db.ForeignKey('post.id',
                                                     ondelete="CASCADE"))
    user = db.relationship("User", back_populates="likes")
    post = db.relationship("Post", back_populates="likes")


class Game(db.Model):
    __tablename__ = 'game'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200), nullable=False)
    win_coef = db.Column(db.Integer, default='2')
    designer_id = db.Column(db.BigInteger, db.ForeignKey('user.id',
                                                     ondelete="CASCADE"))
    designer = db.relationship("User", back_populates="games")
    gambling_transactions = db.relationship("GamblingTransaction",
                                            back_populates="game")


class GamblingTransaction(BaseModel):
    __tablename__ = 'gambling_transaction'
    user_id = db.Column(db.BigInteger, db.ForeignKey('user.id',
                                                     ondelete="SET NULL"))
    game_id = db.Column(db.BigInteger, db.ForeignKey('game.id',
                                                     ondelete="SET NULL"))
    bid = db.Column(db.BigInteger, nullable=False)
    gain = db.Column(db.BigInteger, nullable=False)
    user = db.relationship("User", back_populates='gambling_transactions')
    game = db.relationship("Game", back_populates='gambling_transactions')
