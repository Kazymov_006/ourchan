import datetime
from re import fullmatch, search

from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required, current_user

from . import db, logout_required
from .forms import AuthorizationForm
from .models import User
from .services import save_and_get_path

auth = Blueprint('auth', __name__)

ALLOWED_NICKNAME_SYMBOLS = set()
for symobol_code in range(ord('a'), ord('z') + 1):
    ALLOWED_NICKNAME_SYMBOLS.add(chr(symobol_code))
    ALLOWED_NICKNAME_SYMBOLS.add(chr(symobol_code).upper())
for num in range(10):
    ALLOWED_NICKNAME_SYMBOLS.add(str(num))
ALLOWED_NICKNAME_SYMBOLS.add('_')


@auth.route('/login/', methods=['GET', 'POST'])
@logout_required(redirect_view='main.index')
def login():
    form = AuthorizationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User.query.filter_by(email=form.email.data).first()
        if user and check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            return redirect(url_for('main.index'))
        flash('Email or password are incorrect.')
    return render_template('login.html', form=form)


@auth.route('/signup/', methods=['GET', 'POST'])
@logout_required(redirect_view='main.index')
def signup():
    signup_form = {
        'email': '',
        'first_name': '',
        'last_name': '',
        'nickname': '',
        'date_of_birth': ''
    }
    if request.method == 'POST':
        email = request.form.get('email')
        signup_form['email'] = email
        first_name = request.form.get('first_name')
        signup_form['first_name'] = first_name
        last_name = request.form.get('last_name')
        signup_form['last_name'] = last_name
        nickname = request.form.get('nickname')
        signup_form['nickname'] = nickname
        gender = request.form.get('gender')
        date_of_birth = request.form.get('date_of_birth')
        signup_form['date_of_birth'] = date_of_birth
        password = request.form.get('password')
        password_repeat = request.form.get('password_repeat')

        ok = True
        if email:
            if fullmatch(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b',
                         email):
                user_by_email = User.query.filter_by(email=email).first()
                if user_by_email:
                    flash('User with the same email already exists.')
                    signup_form['email'] = ''
                    ok = False
            else:
                flash('Invalid email address, try again.')
                signup_form['email'] = ''
                ok = False
        else:
            flash('Email is not entered.')
            ok = False

        if nickname:
            if set(nickname) < ALLOWED_NICKNAME_SYMBOLS:
                user_by_nickname = \
                    User.query.filter_by(nickname=nickname).first()
                if user_by_nickname:
                    flash('User with the same nickname already exists.')
                    signup_form['nickname'] = ''
                    ok = False
            else:
                flash('Nickname can only contain latin alphabet symbols,'
                      ' numerals and underscore sign.')
                signup_form['nickname'] = ''
                ok = False
        else:
            flash('Nickname is not entered.')
            ok = False

        if (len(password) < 8 or len(password) > 32
                or not search(r'\d', password)
                or not search(r'[A-Z]', password)
                or not search(r'[a-z]', password)):
            flash('Password has unacceptable format. '
                  'Must be 8-32 characters long '
                  'and include digits, uppercase and lowercase letters.')
            ok = False
        if password != password_repeat:
            flash('Passwords must be the same.')
            ok = False

        try:
            date_of_birth_ = datetime.date.fromisoformat(date_of_birth)
        except ValueError:
            flash('Invalid date of birth')
            signup_form['date_of_birth'] = ''
            ok = False
        else:
            if date_of_birth_ >= datetime.date.today():
                if first_name.lower() != 'john' or last_name.lower() != 'titor':
                    flash('Your name is not John Titor, stop kidding about the '
                          'date of your birth! xD')
                    signup_form['date_of_birth'] = ''
                    ok = False

        if ok:
            first_name = 'Bebra' if first_name == '' else first_name
            last_name = 'Amogusov' if last_name == '' else last_name
            gender = 'custom' if gender not in ('male', 'female') else gender

            new_user = User(
                email=email,
                password=generate_password_hash(password, method='sha256'),
                nickname=nickname,
                first_name=first_name,
                last_name=last_name,
                date_of_birth=date_of_birth,
                gender=gender
            )

            db.session.add(new_user)
            db.session.commit()

            return redirect(url_for('auth.login'))

    return render_template('signup.html', signup_form=signup_form)


@auth.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/profile/edit', methods=['GET', 'POST'])
@login_required
def edit_profile():
    edit_form = {
        'email': current_user.email,
        'first_name': current_user.first_name,
        'last_name': current_user.last_name,
        'nickname': current_user.nickname,
        'date_of_birth': current_user.date_of_birth
    }
    if request.method == 'POST':
        date_of_birth = request.form.get('date_of_birth')
        edit_form['date_of_birth'] = date_of_birth
        first_name = request.form.get('first_name')
        edit_form['first_name'] = first_name
        last_name = request.form.get('last_name')
        edit_form['last_name'] = last_name
        email = request.form.get('email')
        edit_form['email'] = email
        nickname = request.form.get('nickname')
        edit_form['nickname'] = nickname
        gender = request.form.get('gender')
        image = request.files.get('image')

        ok = True
        if email:
            if email != current_user.email:
                if fullmatch(
                        r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b',
                        email):
                    user_by_email = User.query.filter_by(email=email).first()
                    if user_by_email:
                        flash('User with the same email already exists.')
                        edit_form['email'] = current_user.email
                        ok = False
                else:
                    flash('Invalid email address, try again.')
                    edit_form['email'] = current_user.email
                    ok = False
        else:
            flash('Email is not entered.')
            edit_form['email'] = current_user.email
            ok = False

        if nickname:
            if nickname != current_user.nickname:
                if set(nickname) < ALLOWED_NICKNAME_SYMBOLS:
                    user_by_nickname = \
                        User.query.filter_by(nickname=nickname).first()
                    if user_by_nickname:
                        flash('User with the same nickname already exists.')
                        edit_form['nickname'] = current_user.nickname
                        ok = False
                else:
                    flash('Nickname can only contain latin alphabet symbols,'
                          ' numerals and underscore sign.')
                    edit_form['nickname'] = current_user.nickname
                    ok = False
        else:
            flash('Nickname is not entered.')
            edit_form['nickname'] = current_user.nickname
            ok = False

        if date_of_birth != current_user.date_of_birth:
            try:
                date_of_birth_ = datetime.date.fromisoformat(date_of_birth)
            except ValueError:
                flash('Invalid date of birth')
                edit_form['date_of_birth'] = current_user.date_of_birth
                ok = False
            else:
                if date_of_birth_ >= datetime.date.today():
                    flash('Error! Your can\'t be from future! xD')
                    edit_form['date_of_birth'] = current_user.date_of_birth
                    ok = False
        if ok:
            gender = 'custom' if gender not in ('male', 'female') else gender

            img_path = current_user.profile_picture if not image else \
                save_and_get_path(image)

            user = User.query.filter_by(id=current_user.id).first()
            user.email = email
            user.first_name = first_name
            user.last_name = last_name
            user.date_of_birth = date_of_birth
            user.nickname = nickname
            user.gender = gender
            user.profile_picture = img_path

            db.session.commit()
            flash('successfully saved')
            return redirect(url_for('auth.edit_profile'))

    return render_template('edit_profile.html', edit_form=edit_form)


@auth.route('/profile/change_pswd', methods=['GET', 'POST'])
@login_required
def change_password():
    if request.method == 'POST':
        old_password = request.form.get('old_password')
        password = request.form.get('new_password')
        password_repeat = request.form.get('new_password_repeat')

        ok = True
        if not check_password_hash(current_user.password, old_password):
            flash('Wrong password!')
            ok = False

        if (len(password) < 8 or len(password) > 32
                or not search(r'\d', password)
                or not search(r'[A-Z]', password)
                or not search(r'[a-z]', password)):
            flash('New password has unacceptable format. '
                  'Must be 8-32 characters long '
                  'and include digits, uppercase and lowercase letters.')
            ok = False
        if password != password_repeat:
            flash('New passwords must be the same.')
            ok = False

        if ok:
            user = User.query.filter_by(id=current_user.id).first()
            user.password = generate_password_hash(password, method='sha256')
            db.session.commit()
            flash('Password has been successfully changed')

    return render_template('change_password.html')


@auth.route('/profile/<user_nickname>/delete', methods=["POST"])
def delete_user(user_nickname):
    user = User.query.filter_by(nickname=user_nickname).first()
    password = request.form.get('password')
    if user and password:
        if check_password_hash(user.password, password):
            db.session.delete(user)
            db.session.commit()
    return redirect(url_for('main.index'))
