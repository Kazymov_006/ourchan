from functools import wraps

from flask import Flask, redirect, url_for, flash
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user

from . import settings

__version__ = '0.1.0'


db = SQLAlchemy()
migrate = Migrate()


def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = 'ILoveParis1u42yr2c49dr%^@u^&%gj#^&hs45jingo'
    app.config['SQLALCHEMY_DATABASE_URI'] = \
        f'postgresql://{settings.DB_USERNAME}:{settings.DB_PASSWORD}@' \
        f'{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}'
    app.config['FLASK_APP'] = settings.FLASK_APP
    app.config['FLASK_DEBUG'] = settings.FLASK_DEBUG

    db.init_app(app)
    migrate.init_app(app, db)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    from .auth import auth as auth_blueprint
    from .main import main as main_blueprint
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(main_blueprint)

    return app


def logout_required(redirect_view='main.index'):
    def decorator(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if current_user.is_authenticated:
                # flash('Для доступа к этой странице нужно быть '
                #       'неавторизованным.')
                return redirect(url_for(redirect_view))
            return func(*args, **kwargs)

        return decorated_view

    return decorator


app = create_app()


# def send_salary_to(user):
#     with app.app_context():
#         user.net_worth += (
#                 100 * len(user.get_followers_ids()) +
#                 50 * len(user.get_friends_ids()))
#         db.session.commit()
#         db.session.commit()


def send_salary():
    from .models import User
    print("Starting sending of salary")
    with app.app_context():
        all_users = db.session.query(User).all()

        for user in all_users:
            print(f"{user.first_name} {user.last_name} - {user.net_worth}",
                  end=" ")
            user.net_worth += (
                    100 * len(user.get_followers_ids()) +
                    50 * len(user.get_friends_ids()))
            db.session.commit()
            print(f"-> {user.net_worth}")
    print("Salary was sent")

