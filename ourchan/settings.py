from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

FLASK_DEBUG = True
FLASK_APP = 'ourchan'

DB_USERNAME = 'ourchandb'
DB_PASSWORD = 'ourchandb'
DB_HOST = 'localhost'
DB_PORT = 5432
DB_NAME = 'ourchandb'

MEDIA_DIR = 'media'
MEDIA_DIR_ABS = BASE_DIR / 'ourchan' / 'static' / 'media'
