from random import randint

from flask import (
    Blueprint,
    render_template,
    request,
    flash,
    redirect,
    url_for,
    make_response,
    jsonify
)
from flask_login import login_required, current_user
from sqlalchemy import text

from . import db, logout_required
from .models import Post, User, UserUserSub, Image, Like, Game, \
    GamblingTransaction
from .services import (
    get_users_top,
    search_users_query,
    WrongExtensionForImageError,
    save_and_get_path
)

main = Blueprint('main', __name__)


@main.route('/')
@logout_required(redirect_view='main.news')
def index():
    return render_template('index.html')


@main.route('/profile/')
@login_required
def profile():
    return redirect(url_for('main.profile_of',
                            user_nickname=current_user.nickname,
                            page_id=1))


@main.route('/profile/<user_nickname>/')
@login_required
def profile_of(user_nickname):
    return redirect(url_for('main.profile_page_of',
                            user_nickname=user_nickname,
                            page_id=1))


@main.route('/profile/<user_nickname>/page=<int:page_id>')
@login_required
def profile_page_of(user_nickname, page_id: int):
    user_nickname = \
        current_user.nickname if user_nickname == '' else user_nickname
    user = User.query.filter_by(nickname=user_nickname).first()
    per_page = 10
    posts = Post.query.filter_by(user_id=user.id, root_id=None).order_by(
        Post.created_at.desc())\
        .paginate(page_id, per_page, error_out=False)
    return render_template('profile.html', posts=posts, user=user)


@main.route('/profile', methods=['POST'])
@login_required
def add_post():
    user_id = current_user.id
    text = request.form.get('text')
    images = request.files.getlist('images')
    img_paths = []
    for image in images:
        try:
            path = save_and_get_path(image)
            img_paths.append(path)
        except WrongExtensionForImageError:
            flash("You uploaded file of non-image type. Unrecognized files "
                  "were skipped")
        except Exception as e:
            print(e.__repr__())
            pass

    if text == '':
        flash('Empty posts are not allowed')
        return redirect(url_for('main.profile'))

    if len(text) > 5000:
        flash('Text is too long')
        return redirect(url_for('main.profile'))

    new_post = Post(
        text=text,
        user_id=user_id
    )
    db.session.add(new_post)
    db.session.commit()

    for img_path in img_paths:
        new_image = Image(
            post_id=new_post.id,
            file=img_path
        )
        db.session.add(new_image)
        db.session.commit()

    return redirect(url_for('main.profile'))


@main.route('/news/')
@login_required
def news():
    return redirect(url_for('main.news_page', page_id=1))


@main.route('/news/page=<page_id>')
@login_required
def news_page(page_id=1):
    try:
        page_id = int(page_id)
    except ValueError:
        page_id = 1
    page_size = 10
    news_ = current_user.get_news()
    max_id = len(news_) // page_size + (len(news_) // page_size > 0) - 1
    if page_id == -1:
        page_id = max_id
    elif page_id > 0:
        page_id -= 1
    else:
        page_id = 0
    news_ = news_[page_size * page_id:page_size * (page_id + 1)]
    return render_template('news.html',
                           news=news_,
                           page_id=page_id,
                           max_id=max_id)


@main.route('/friends/')
@login_required
def friends():
    return redirect(url_for('main.friend_list', nickname=current_user.nickname))


@main.route('/profile/<nickname>/friends/')
@login_required
def friend_list(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user:
        return render_template('friends.html', user=user)
    flash('This user doesn\'t exist')
    return redirect(url_for('main.index'))


@main.route('/gambling/')
@login_required
def gambling():
    games = Game.query.filter_by(designer_id=current_user.id).all()
    return render_template('gambling.html', games=games)


@main.route('/gambling/id<game_id>', methods=['GET', 'POST'])
@login_required
def game(game_id):
    game_ = Game.query.filter_by(id=game_id).first()
    # it is normal to get someone else's game
    if not game_:
        flash("Requesting game doesn't exist")
        return redirect(url_for('main.gambling'))
    if request.method == 'POST':
        bid = request.form.get('bid', 0)
        try:
            bid = int(bid)
        except ValueError:
            flash("Incorrect value for bid")
            return redirect(url_for('main.game', game_id=game_id))

        if bid <= 0:
            flash("Too small bid")
            return redirect(url_for('main.game', game_id=game_id))
        if bid > current_user.net_worth:
            flash("not enough money(")
            return redirect(url_for('main.game', game_id=game_id))

        # Game
        gain = 0
        events = int(game_.win_coef)
        if randint(1, events) == randint(1, events):
            # win condition passed
            gain = bid * game_.win_coef

        # pay
        user = User.query.filter_by(id=current_user.id).first()
        user.net_worth += gain - bid
        new_gambling_transaction = GamblingTransaction(
            user_id=user.id,
            game_id=game_id,
            bid=bid,
            gain=gain
        )
        db.session.add(new_gambling_transaction)
        db.session.commit()
    return render_template('game.html', game=game_)


@main.route('/gambling/add_game', methods=['POST'])
@login_required
def add_game():
    name = request.form.get('name')
    win_coef = request.form.get('win_coef')
    if not name or not win_coef:
        flash("request is broke")
        return redirect(url_for('main.gambling'))
    try:
        win_coef = int(win_coef)
    except ValueError:
        flash("Only integer coefficients are allowed")
        return redirect(url_for('main.gambling'))
    except Exception as e:
        print(repr(e))
        flash("Unknown error")
        return redirect(url_for('main.gambling'))
    if win_coef < 2 or win_coef > 1000:
        flash("Win coefficient is out of allowed range")
        return redirect(url_for('main.gambling'))
    game__ = \
        Game.query.filter_by(designer_id=current_user.id, name=name).first()
    if game__:
        flash("You already have a game with the same name.")
        return redirect(url_for('main.gambling'))

    new_game = Game(
        name=name,
        win_coef=win_coef,
        designer_id=current_user.id
    )
    db.session.add(new_game)
    db.session.commit()
    return redirect(url_for('main.gambling'))


@main.route('/top_users/')
def top_users():
    users_top = get_users_top()
    return render_template('top_users.html', users_top=users_top)


@main.route('/search/')
def search():
    user_name = request.args.get('query', '')
    searched_users = []
    if user_name:
        searched_users = search_users_query(user_name).all()
    else:
        flash("Yoy have requested an empty query!")
    return render_template('search.html', searched_users=searched_users,
                           q_txt=user_name)


@main.route('/add_friend/', methods=['GET', 'POST'])
@login_required
def add_friend():
    if request.method == 'POST':
        form = request.form
        name_act = form.get('name_action')
        if not name_act or ';' not in name_act:
            flash('Unrecognized request')
            return redirect(url_for('main.profile'))
        nickname, action = name_act.split(';')
        if nickname == current_user.nickname:
            flash('You can\'t friend yourself')
            return redirect(url_for('main.profile'))

        user = User.query.filter_by(nickname=nickname).first()
        if not user:
            flash('User you want to friend does not exist!')
            return redirect(url_for('main.profile'))

        if user.id in current_user.get_all_followings_ids():
            if action == 'UNFOLLOW':
                sub = UserUserSub.query.filter_by(follower_id=current_user.id,
                                                  following_id=user.id).first()
                db.session.delete(sub)
                db.session.commit()
            else:
                flash('Wrong action, you\'re following this user.')
        else:
            if action == 'FOLLOW':
                sub = UserUserSub(
                    follower_id=current_user.id,
                    following_id=user.id
                )
                db.session.add(sub)
                db.session.commit()
            else:
                flash('Wrong action, you\'re not following this user.')
        return redirect(url_for('main.profile_of', user_nickname=nickname))

    flash('There is nobody to friend!')
    return redirect(url_for('main.profile'))


@main.route('/like', methods=['POST'])
@login_required
def like():
    post_id = request.form.get('post_id')
    if not post_id:
        return "error", 404
    try:
        post_id = int(post_id)
    except ValueError:
        return "error", 404
    Post.query.filter_by(id=post_id).first_or_404()
    like_ = Like.query.filter_by(user_id=current_user.id,
                                 post_id=post_id).first()
    if like_:
        db.session.delete(like_)
        db.session.commit()
    else:
        new_like = Like(
            post_id=post_id,
            user_id=current_user.id
        )
        db.session.add(new_like)
        db.session.commit()
    return "success", 200


@main.route('/posts/<post_id>', methods=['GET', 'POST'])
@login_required
def comment(post_id):
    try:
        post_id = int(post_id)
    except ValueError:
        flash("Request is broken")
        return redirect(url_for('main.news'))
    post = Post.query.filter_by(id=post_id).first()

    if not post:
        flash("Requested post does not exist.")
        return redirect(url_for('main.news'))

    if request.method == "POST":
        text = request.form.get('text')
        images = request.files.getlist('images')
        root_id = request.form.get('root_id')
        replying_to_post_id = request.form.get('replying_to_post_id')
        user_id = current_user.id
        try:
            root_id = int(root_id)
            replying_to_post_id = int(replying_to_post_id)
        except ValueError:
            flash("Request is broken.")
            return redirect(url_for('main.profile'))
        root = Post.query.filter_by(id=root_id).first()
        replying_to_post = Post.query.filter_by(id=replying_to_post_id).first()
        if not root:
            flash('Post you are commenting does not exist')
            return redirect(url_for('main.profile'))
        if not replying_to_post:
            flash('Post you are replying on does not exist')
            return redirect(url_for('main.profile'))
        if root.root_id:
            root_id = root.root_id
        img_paths = []
        if images:
            for image in images:
                try:
                    path = save_and_get_path(image)
                    img_paths.append(path)
                except WrongExtensionForImageError:
                    flash("You uploaded file of non-image type. Unrecognized files "
                          "were skipped")
                except Exception as e:
                    print(e.__repr__())
                    pass

        if len(text) > 5000:
            flash('Text is too long')
            return redirect(url_for('main.profile'))

        if text == '':
            flash('Empty posts are not allowed')
            return redirect(url_for('main.profile'))

        print(f"[repl to post id]{replying_to_post_id}")
        print(f"[root id]{root_id}")
        new_post = Post(
            text=text,
            user_id=user_id,
            replying_to_post_id=replying_to_post_id,
            root_id=root_id
        )
        db.session.add(new_post)
        db.session.commit()

        for img_path in img_paths:
            new_image = Image(
                post_id=new_post.id,
                file=img_path
            )
            db.session.add(new_image)
            db.session.commit()

        return redirect(url_for('main.comment', post_id=post.id))

    return render_template('comments.html', post=post)
