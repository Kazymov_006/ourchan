from apscheduler.schedulers.background import BackgroundScheduler
from ourchan import app, send_salary, settings

if __name__ == '__main__':
    salary_job = BackgroundScheduler(daemon=True)
    salary_job.add_job(send_salary, 'cron', day='*', hour=23,
                       minute=59, jitter=3600)
    salary_job.start()
    app.run(use_reloader=False, debug=settings.FLASK_DEBUG)
