from random import randint

import pytest
import sqlalchemy as sa
from werkzeug.security import generate_password_hash

from ourchan import create_app, db
from ourchan.models import User, UserUserSub, Post


@pytest.fixture(scope='session')
def flask_app():
    app = create_app()
    app.testing = True
    return app


@pytest.fixture(scope='session')
def new_user(db_session):
    user = User(
        email="testemail@gmail.com",
        password=generate_password_hash("TestPassword123", method='sha256'),
        nickname='test_nickname',
        first_name='Test',
        last_name='Testov',
        date_of_birth='2020-02-02',
        gender='custom'
    )
    db_session.add(user)
    db_session.commit()
    return user


@pytest.fixture(scope='function')
def test_client(flask_app):
    with flask_app.test_client() as test_client:
        with flask_app.app_context():
            yield test_client


@pytest.fixture(scope='function')
def logged_test_client(flask_app):
    data = {
        'email': f'test{randint(0, 1000000)}@gmail.com',
        'first_name': 'Test',
        'last_name': 'Testov',
        'nickname': 'test740028317404702389021374202138472907487983_nick',
        'date_of_birth': '2000-01-01',
        'gender': 'male',
        'password': 'Password123',
        'password_repeat': 'Password123'
    }
    with flask_app.test_client() as test_client:
        with flask_app.app_context():
            test_client.post('/signup/', data=data, follow_redirects=True)
            test_client.post('/login/', data={'email': data['email'],
                                              'password': data['password']},
                             follow_redirects=True)
            yield test_client
            # deletion
            test_client.post(f"/profile/{data['nickname']}/delete",
                             data={'password': 'Password123'},
                             follow_redirects=True)


@pytest.fixture(scope='session')
def db_session(flask_app):
    """
    Creates a new database session for a test. Note you must use this fixture
    if your test connects to db.

    Here we not only support commit calls but also rollback calls in tests.
    """
    with flask_app.app_context():
        connection = db.engine.connect()
        transaction = connection.begin()

        options = dict(bind=connection, binds={})
        session = db.create_scoped_session(options=options)

        session.begin_nested()

        # session is actually a scoped_session
        # for the `after_transaction_end` event, we need a session instance to
        # listen for, hence the `session()` call
        @sa.event.listens_for(session(), 'after_transaction_end')
        def restart_savepoint(sess, trans):
            if trans.nested and not trans._parent.nested:
                session.expire_all()
                session.begin_nested()

        db.session = session

        yield session

        session.remove()
        transaction.rollback()
        connection.close()


@pytest.fixture(scope='function')
def filled_db_session(db_session):
    user1 = User(
        email="testemail1@gmail.com",
        password=generate_password_hash("TestPassword123", method='sha256'),
        nickname='test_nickname1',
        first_name='Test1',
        last_name='Testov',
        date_of_birth='2020-02-02',
        gender='custom'
    )
    user2 = User(
        email="testemail2@gmail.com",
        password=generate_password_hash("TestPassword123", method='sha256'),
        nickname='test_nickname2',
        first_name='Test2',
        last_name='Testov',
        date_of_birth='2020-02-02',
        gender='custom'
    )
    user3 = User(
        email="testemail3@gmail.com",
        password=generate_password_hash("TestPassword123", method='sha256'),
        nickname='test_nickname3',
        first_name='Test3',
        last_name='Testov',
        date_of_birth='2020-02-02',
        gender='custom'
    )
    user4 = User(
        email="testemail4@gmail.com",
        password=generate_password_hash("TestPassword123", method='sha256'),
        nickname='test_nickname4',
        first_name='Test4',
        last_name='Testov',
        date_of_birth='2020-02-02',
        gender='custom'
    )
    db_session.add(user1)
    db_session.add(user2)
    db_session.add(user3)
    db_session.add(user4)
    db_session.commit()
    rel1 = UserUserSub(
        follower_id=user1.id,
        following_id=user2.id
    )
    rel2 = UserUserSub(
        follower_id=user1.id,
        following_id=user3.id
    )
    rel3 = UserUserSub(
        follower_id=user3.id,
        following_id=user1.id
    )
    rel4 = UserUserSub(
        follower_id=user4.id,
        following_id=user1.id
    )
    db_session.add(rel1)
    db_session.add(rel2)
    db_session.add(rel3)
    db_session.add(rel4)
    db_session.commit()

    post1 = Post(
        user_id=user2.id,
        text="This is test post"
    )
    db_session.add(post1)
    db_session.commit()
    post2 = Post(
        user_id=user3.id,
        text="This is test comment",
        root_id=post1.id,
        replying_to_post_id=post1.id
    )
    db_session.add(post2)
    db_session.commit()
    post3 = Post(
        user_id=user4.id,
        text="This is test reply on comment",
        root_id=post1.id,
        replying_to_post_id=post2.id
    )
    db_session.add(post3)
    db_session.commit()
    yield db_session
    db_session.delete(user1)
    db_session.delete(user2)
    db_session.delete(user3)
    db_session.delete(user4)
    db_session.commit()


@pytest.fixture(scope='session')
def actual_db(flask_app):
    with flask_app.app_context():
        yield db
