import datetime
from random import randint

from werkzeug.security import check_password_hash, generate_password_hash

from ourchan import __version__
import ourchan
from ourchan import services, auth, main, settings, models
from ourchan.models import User, Post, Game, UserUserSub
from .conftest import (new_user,
                       test_client,
                       logged_test_client,
                       db_session,
                       filled_db_session,
                       actual_db)


def test_version():
    assert __version__ == '0.1.0'


def test_new_user(new_user):
    """
    GIVEN a User model
    WHEN a new User is created
    THEN check the email, password, nickname, gender, first_name, last_name,
    and is_authenticated fields are defined correctly
    """
    assert new_user.email == 'testemail@gmail.com'
    assert check_password_hash(new_user.password, 'TestPassword123')
    assert new_user.nickname == 'test_nickname'
    assert new_user.gender == 'custom'
    assert new_user.first_name == 'Test'
    assert new_user.last_name == 'Testov'
    assert new_user.net_worth == 0
    assert new_user.is_authenticated


def test_new_post(filled_db_session):
    """
    GIVEN a Post model
    WHEN a new Post is created
    THEN check fields are defined correctly
    """
    user = filled_db_session.query(User).filter_by(
        nickname='test_nickname2').first()
    new_post = filled_db_session.query(Post).filter_by(user_id=user.id).first()
    assert new_post.text == 'This is test post'
    assert new_post.user_id == user.id
    assert new_post.root is None
    assert new_post.replying_to_post_id is None
    assert new_post.user.id == user.id


def test_index_get(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check that response is valid
    """
    response = test_client.get('/')
    assert response.status_code == 200
    assert b"Welcome to the" in response.data


def test_index_post(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (POST)
    THEN check that a '405' status code is returned
    """
    response = test_client.post('/')
    assert response.status_code == 405


def test_signup_get(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the page '/signup' is requested (GET)
    THEN chek that response is valid
    """
    response = test_client.get('/signup', follow_redirects=True)
    assert b'Registration' in response.data


def test_signup_and_login_post(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the page '/signup' is requested (Post)
    THEN chek that response is valid
    WHEN the page '/login' is requested (Post)
    THEN chek that response is valid
    WHEN the page 'profile/<user_nickname>/delete' is requested (Post)
    THEN chek that response is valid
    """
    data = {
        'email': f'test{randint(0, 1000000)}@gmail.com',
        'first_name': 'Test',
        'last_name': 'Testov',
        'nickname': f'test{randint(0, 1000000)}_nick',
        'date_of_birth': '2000-01-01',
        'gender': 'male',
        'password': 'Password123',
        'password_repeat': 'Password123'
    }

    # signup
    response = test_client.post('/signup/', data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Authorization' in response.data

    # check if handles existing email and nickname
    response = test_client.post('/signup/', data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'User with the same email already exists.' in response.data
    assert b'User with the same nickname already exists.' in response.data

    # login
    response = test_client.post('/login/', data={'email': data['email'],
                                                 'password': data['password']},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'News' in response.data

    # deletinon of test user
    response = test_client.post(f"/profile/{data['nickname']}/delete",
                                data={'password': 'Password123'},
                                follow_redirects=True)
    assert response.status_code == 200

    # check that user has been deleted
    response = test_client.post('/login/', data={'email': data['email'],
                                                 'password': data['password']},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Authorization' in response.data
    assert b'Email or password are incorrect.' in response.data

    # check if handles incorrect email, nickname, password, date of birth
    data['email'] = 'incorrect email'
    data['nickname'] = 'incorrect nicname'
    data['password'] = 'incorrect password'
    data['password_repeat'] = '123notright'
    data['date_of_birth'] = (datetime.date.today() + datetime.timedelta(
        days=1)).isoformat()
    response = test_client.post('/signup/', data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Invalid email address, try again.' in response.data
    assert b'Nickname can only contain latin a' in response.data
    assert b'Password has unacceptable format.' in response.data
    assert b'Passwords must be the same.' in response.data
    assert b'Your name is not John Titor' in response.data


def test_logout_get(logged_test_client):
    # I have tired of writing docs((
    response = logged_test_client.get('/logout/', follow_redirects=True)
    assert response.status_code == 200
    assert b'Authorization' not in response.data
    assert b'This is something new' in response.data


def test_logout_post(logged_test_client):
    response = logged_test_client.post('/logout', follow_redirects=True)
    assert response.status_code == 405


def test_profile_edit_not_authenticated(test_client):
    response = test_client.get('/profile/edit')
    assert response.status_code == 302


def test_profile_edit_get(logged_test_client):
    response = logged_test_client.get('/profile/edit')
    assert response.status_code == 200


def test_profile_edit_post(logged_test_client, actual_db):
    data = {
        'email': f'test{randint(1000000, 2000000)}@gmail.com',
        'first_name': 'TestEdit',
        'last_name': 'TestovEdit',
        'nickname': 'test740028317404702389021374202138472907487983_nick',
        'date_of_birth': '2000-01-01',
        'gender': 'male'
    }
    response = logged_test_client.post('/profile/edit', data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'successfully saved' in response.data

    user = actual_db.session.query(User).filter_by(nickname=data[
        'nickname']).first()
    assert user.email == data['email']
    assert user.first_name == data['first_name']

    # check if fails on incorrect data
    data = {
        'email': f'incorrect',
        'first_name': 'incorrect',
        'last_name': 'incorrect',
        'nickname': f'in cor rect',
        'date_of_birth':
            (datetime.date.today() + datetime.timedelta(days=1)).isoformat(),
        'gender': 'male'
    }
    response = logged_test_client.post('/profile/edit', data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Error! Your can' in response.data
    assert b'Nickname can only contain latin a' in response.data
    assert b'Invalid email address, try again.' in response.data


# main functions test
def test_profile_get(logged_test_client):
    response = logged_test_client.get('/profile', follow_redirects=True)
    assert response.status_code == 200
    assert b'Test Testov' in response.data
    assert b'Add new post' in response.data


def test_profile_post(logged_test_client, actual_db):
    data = {'text': ''}
    response = logged_test_client.post('/profile',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Empty posts are not allowed' in response.data

    data['text'] = 'e' * 5001
    response = logged_test_client.post('/profile',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Text is too long' in response.data

    data['text'] = 'Test post number one'
    response = logged_test_client.post('/profile',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Test post number one' in response.data


def test_news_get(logged_test_client):
    response = logged_test_client.get('/news/', follow_redirects=True)
    assert response.status_code == 200
    assert b'News' in response.data

    response = logged_test_client.get('/news/page=qwerty',
                                      follow_redirects=True)
    assert response.status_code == 200
    assert b'News' in response.data

    response = logged_test_client.get('/news/page=-1',
                                      follow_redirects=True)
    assert response.status_code == 200
    assert b'Last page' not in response.data

    response = logged_test_client.get('/news/page=-123',
                                      follow_redirects=True)
    assert response.status_code == 200
    assert b'News' in response.data


def test_news_post(logged_test_client):
    response = logged_test_client.post('/news/', follow_redirects=True)
    assert response.status_code == 405

    response = logged_test_client.post('/news/page=-12', follow_redirects=True)
    assert response.status_code == 405


def test_friends_get(logged_test_client):
    response = logged_test_client.get('/friends/', follow_redirects=True)
    assert response.status_code == 200
    assert b'Friends' in response.data
    assert b'Followers' in response.data
    assert b'Following' in response.data

    response = logged_test_client.get('/profile/non-existing-nickname/friends',
                                      follow_redirects=True)
    assert response.status_code == 200
    assert b"This user does" in response.data
    assert b'News' in response.data


def test_friends_post(logged_test_client):
    response = logged_test_client.post('/friends/', follow_redirects=True)
    assert response.status_code == 405

    response = logged_test_client.post('/friends/', follow_redirects=True)
    assert response.status_code == 405


def test_gambling_get_and_post(logged_test_client, actual_db):
    response = logged_test_client.get('/gambling', follow_redirects=True)
    assert response.status_code == 200
    assert b'Games for gambling' in response.data
    assert b'Add new game' in response.data

    response = logged_test_client.get('/gambling/id1234', follow_redirects=True)
    assert response.status_code == 200
    assert b"Requesting game does" in response.data

    # create game
    user = actual_db.session.query(User).filter_by(
        nickname='test740028317404702389021374202138472907487983_nick').first()
    new_game = Game(
        name='test game',
        win_coef=2,
        designer_id=user.id
    )
    actual_db.session.add(new_game)
    actual_db.session.commit()

    response = logged_test_client.get(f'/gambling/id{new_game.id}',
                                      follow_redirects=True)
    assert response.status_code == 200
    assert b"test game" in response.data

    data = {'bid': 'qwerty'}
    response = logged_test_client.post(f'/gambling/id{new_game.id}',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Incorrect value for bid' in response.data

    data['bid'] = 0
    response = logged_test_client.post(f'/gambling/id{new_game.id}',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Too small bid' in response.data

    data['bid'] = 100
    response = logged_test_client.post(f'/gambling/id{new_game.id}',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'not enough money' in response.data

    user.net_worth = 100
    actual_db.session.commit()
    response = logged_test_client.post(f'/gambling/id{new_game.id}',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'not enough money' not in response.data
    assert b'Too small bid' not in response.data
    assert b'Incorrect value for bid' not in response.data


def test_add_game_get(logged_test_client):
    response = logged_test_client.get('gambling/add_game')
    assert response.status_code == 405


def test_add_game_post(logged_test_client):
    data = {
        'name': 'Test Game',
        'win_coef': ''
    }
    response = logged_test_client.post('/gambling/add_game',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'request is broke' in response.data

    data['win_coef'] = 'asd'
    response = logged_test_client.post('/gambling/add_game',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Only integer coefficients are allowed' in response.data

    data['win_coef'] = '1'
    response = logged_test_client.post('/gambling/add_game',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Win coefficient is out of allowed range' in response.data

    data['win_coef'] = '1001'
    response = logged_test_client.post('/gambling/add_game',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Win coefficient is out of allowed range' in response.data

    data['win_coef'] = '100'
    response = logged_test_client.post('/gambling/add_game',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Win coefficient is out of allowed range' not in response.data
    assert b'Only integer coefficients are allowed' not in response.data
    assert b'request is broke' not in response.data
    assert b'You already have a game with the same name.' not in response.data

    response = logged_test_client.post('/gambling/add_game',
                                       data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'You already have a game with the same name.' in response.data


def test_top_users_get(logged_test_client):
    response = logged_test_client.get('/top_users/')
    assert response.status_code == 200
    assert b'Top 20 richest users' in response.data


def test_top_users_post(logged_test_client):
    response = logged_test_client.post('/top_users/')
    assert response.status_code == 405


def test_search_get(logged_test_client):
    response = logged_test_client.get('/search', follow_redirects=True)
    assert response.status_code == 200
    assert b'Yoy have requested an empty query!' in response.data

    q = 'Te'
    response = logged_test_client.get(f'/search/?query={q}',
                                      follow_redirects=True)
    assert response.status_code == 200
    assert b'Test' in response.data


# Idk why but if I uncomment this covered by tests statements will reduce
def test_search_post(test_client):
    response = test_client.post('/search/')
    assert response.status_code == 405


def test_add_friend_get(logged_test_client):
    response = logged_test_client.get('/add_friend/', follow_redirects=True)
    assert response.status_code == 200
    assert b'There is nobody to friend!' in response.data


def test_add_friend_post(logged_test_client, actual_db):
    data = {'name_action': 'asdf '}
    response = logged_test_client.post('/add_friend/', data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Unrecognized request' in response.data

    data['name_action'] = \
        'test740028317404702389021374202138472907487983_nick;amogus'
    response = logged_test_client.post('/add_friend/', data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b't friend yourself' in response.data

    data['name_action'] = 'amogus;bebra'
    response = logged_test_client.post('/add_friend/', data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'User you want to friend does not exist!' in response.data

    new_user = User(
        email="blank@gmail.com",
        password=generate_password_hash("TestPassword123", method='sha256'),
        nickname='blank',
        first_name='Blank',
        last_name='Blank',
        date_of_birth='2020-02-02',
        gender='custom'
    )
    actual_db.session.add(new_user)
    actual_db.session.commit()

    data['name_action'] = 'blank;UNFOLLOW'
    response = logged_test_client.post('/add_friend/', data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Wrong action' in response.data

    data['name_action'] = 'blank;FOLLOW'
    response = logged_test_client.post('/add_friend/', data=data,
                                       follow_redirects=True)
    assert response.status_code == 200
    assert b'Wrong action' not in response.data

    uus = actual_db.session.query(UserUserSub).filter_by(
        following_id=new_user.id).first()
    assert uus


def test_like_get(logged_test_client):
    response = logged_test_client.get('/like')
    assert response.status_code == 405


def test_like_post(logged_test_client, actual_db):
    data = {'post_id': 'qwerty'}
    response = logged_test_client.post('/like')
    assert response.status_code == 404
    response = logged_test_client.post('/like', data=data)
    assert response.status_code == 404

    data['post_id'] = '1231232123'
    response = logged_test_client.post('/like', data=data)
    assert response.status_code == 404

    user = actual_db.session.query(User).filter_by(
        nickname='test740028317404702389021374202138472907487983_nick').first()
    new_post = Post(
        text='Test post',
        user_id=user.id
    )
    actual_db.session.add(new_post)
    actual_db.session.commit()

    data['post_id'] = new_post.id
    response = logged_test_client.post('/like', data=data)
    assert response.status_code == 200
    response = logged_test_client.post('/like', data=data)
    assert response.status_code == 200


def test_comment_get(logged_test_client, actual_db):
    response = logged_test_client.get('/posts/')
    assert response.status_code == 404

    pid = 2131902382
    response = logged_test_client.get(f'/posts/{pid}', follow_redirects=True)
    assert response.status_code == 200
    assert b'Requested post does not exist.' in response.data

    pid = 'qwerty'
    response = logged_test_client.get(f'/posts/{pid}', follow_redirects=True)
    assert response.status_code == 200
    assert b'Request is broken' in response.data

    user = actual_db.session.query(User).filter_by(
        nickname='test740028317404702389021374202138472907487983_nick').first()
    new_post = Post(
        text='Test new post',
        user_id=user.id
    )
    actual_db.session.add(new_post)
    actual_db.session.commit()

    pid = new_post.id
    response = logged_test_client.get(f'/posts/{pid}', follow_redirects=True)
    assert response.status_code == 200
    assert b'Test new post' in response.data


def test_comment_post(logged_test_client, actual_db):
    user = actual_db.session.query(User).filter_by(
        nickname='test740028317404702389021374202138472907487983_nick').first()
    new_post = Post(
        text='Test new post',
        user_id=user.id
    )
    actual_db.session.add(new_post)
    actual_db.session.commit()

    pid = new_post.id
    data = {
        'text': '',
        'root_id': 'qwerty',
        'replying_to_post_id': 'qwerty'
    }
    response = logged_test_client.post(f'/posts/{pid}',
                                       data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Request is broken.' in response.data

    data['root_id'] = 31231920
    data['replying_to_post_id'] = 13213123
    response = logged_test_client.post(f'/posts/{pid}',
                                       data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Post you are commenting does not exist' in response.data

    data['root_id'] = new_post.id
    response = logged_test_client.post(f'/posts/{pid}',
                                       data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Post you are replying on does not exist' in response.data

    data['replying_to_post_id'] = new_post.id
    response = logged_test_client.post(f'/posts/{pid}',
                                       data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Empty posts are not allowed' in response.data

    data['text'] = 'a'*5001
    response = logged_test_client.post(f'/posts/{pid}',
                                       data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Text is too long' in response.data

    data['replying_to_post_id'] = ''
    data['text'] = 'Test comment'
    response = logged_test_client.post(f'/posts/{pid}',
                                       data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Request is broken' in response.data

    data['replying_to_post_id'] = new_post.id
    response = logged_test_client.post(f'/posts/{pid}',
                                       data=data, follow_redirects=True)
    assert response.status_code == 200
    assert b'Test comment' in response.data
