# OurChan

Social network without messenger, but with gambling

## instruction

- `git clone https://gitlab.com/Kazymov_006/ourchan.git` clone rep
- `docker-compose up -d` set up database.
if you aren't using docker, create it and set up `settings.py`
- `poetry shell` - init virtual environment
- `poetry install` - install dependencies
- `flask db upgrade` - upgrade data base to actual version
- `python start.py` - run start.py file to launch site
